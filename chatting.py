#!/usr/bin/env python3
"""
Alice sends messages to Bob, who responds to them. When Bob receives a new message from Alice while he was about to
generate a response, he restarts response generation with the new message included in his prompt. 
This is implemented using a global queue and an event. Alice and Bob are implemented as classes with methods to send 
and receive messages. Alice and Bob are run in separate threads. The main thread is used to send messages to Alice 
and Bob. The main thread also listens for user input to stop the program. When the program is stopped, the threads 
are terminated.
"""

import threading
import queue

from time import sleep
from speaker import VoiceSpeaker as Speaker
from responder import VoiceResponder as Responder
# from speaker import Speaker
# from responder import Responder


# Thread function for Alice
def alice_thread():
    for message in speaker.generate_messages():
        # Send the message to Bob
        message_queue.put(message)
        # Tell Bob to restart response generation
        restart_event.set()
        # can_speak_event.set()


# Thread function for Bob
def bob_thread():
    messages = ""

    while True:
        # Wait for a new message from Alice
        if message_queue.empty():
            sleep(0.01)
            continue
        message = message_queue.get()

        # Concatenate the received messages
        messages += message

        # Continue response generation until completion
        restart_event.clear()
        response = ""
        sentence = ""
        for token in responder.generate(messages):
            responder.display_token(token)
            sentence += token
            if token in [".", "?", "!", "\n"]:
                response += sentence
                sentence_queue.put(sentence)
                sentence = ""
            # Check if a new message arrived during response generation
            if restart_event.is_set():
                restart_event.clear()
                break
        else:
            response += sentence
            sentence_queue.put(sentence)
            sentence = ""

            # Response generation completed
            restart_event.clear()
            responder.send_response(messages, response)
            messages = ""                


def say_sentence_thread():
    while True:
        if sentence_queue.empty():
            sleep(0.01)
            continue
        sentence = sentence_queue.get()
        # can_speak_event.wait()
        # speaking_event.clear()
        responder.synthesizer.speak(sentence)
        # speaking_event.set()
        # can_speak_event.clear()


if __name__ == "__main__":
    langauge = "russian"
    settings_dict = {
        "english": {"model": "vosk-model-en-us-0.22", "voice": "Joanna", "responder": "Jenny", "speaker": "Arthur"},
        "russian": {"model": "vosk-model-small-ru-0.22", "voice": "Tatyana", "responder": "Таня", "speaker": "Артур"}
    }
    settings = settings_dict[langauge]

    # Global queue for message passing between Alice and Bob
    message_queue = queue.Queue()

    # Global queue for passing a sentence to be spoken
    sentence_queue = queue.Queue()

    # Event to signal Bob to restart response generation
    restart_event = threading.Event()

    # Event to signal Bob to stop speaking
    # can_speak_event = threading.Event()
    # speaking_event = threading.Event()
    # speaking_event.set()

    speaker = Speaker(**settings)
    responder = Responder(**settings)

    try:
        # Start Alice and Bob threads
        alice = threading.Thread(target=alice_thread)
        bob = threading.Thread(target=bob_thread)
        say_sentence = threading.Thread(target=say_sentence_thread)
        alice.start()
        bob.start()
        say_sentence.start()
        while True:
            pass
    except KeyboardInterrupt:
        pass
    finally:
        speaker.terminate()
        responder.terminate()