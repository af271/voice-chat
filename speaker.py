import pyaudio
from time import sleep
from vosk import Model, KaldiRecognizer

class Speaker:

    def __init__(self, **kwargs):
        pass

    def generate_messages(self):
        texts = [
            "привет таня как дела", 
            "я сегодня кормил слонов", 
            "слоны довольно маленькие азиатские", 
            "это был заповедник в тайланде на острове Самуи, куда я ездил сегодня",
        ]
        while True:
            for t in texts:
                yield t + ". "
                sleep(3)
            sleep(7)

    def stop(self):
        print("\nstop!")

    def resume(self):
        print("\nresume!")

    def terminate(self):
        print("\nterminating")


class VoiceSpeaker(Speaker):

    def __init__(self, speaker="", **kwargs):
        self.speaker_name = speaker
        model = Model(kwargs["model"])
        self.recognizer = KaldiRecognizer(model, 16000)

        mic = pyaudio.PyAudio()
        self.stream = mic.open(format=pyaudio.paInt16, channels=1, rate=16000, input=True, frames_per_buffer=8192, input_device_index=7, output_device_index=8)

    def generate_messages(self):
        self.stream.start_stream()
        while True:
            # if not event.is_set():
            #     self.stop()
            #     event.wait()
            #     self.resume()
            data = self.stream.read(1024)

            if not self.recognizer.AcceptWaveform(data):
                continue

            # with open(filename, "rb") as audio_file:
            #     transcript = openai.Audio.transcribe("whisper-1", audio_file).text
            #     print(transcript)
            transcript = self.recognizer.Result()[14:-3]
            if transcript[:4] == "the ":
                transcript = transcript[4:]
            if transcript[-3:] == "the":
                transcript = transcript[:-3]

            if len(transcript) < 5:
                continue
            print(f"\n\033[93m[{self.speaker_name}]: {transcript}\033[0m")  # yellow
            yield transcript + ". "

    def stop(self):
        print("\nstop!")
        self.stream.stop_stream()

    def resume(self):
        print("resume!")
        self.stream.start_stream()

    def terminate(self):
        self.stream.close()
