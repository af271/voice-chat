
1. Make a .env file and insert your openai key
OPENAI_API_KEY=...
OPENAI_MODEL_ENGINE="gpt-4"

2. Setup a file role.txt with the role of the chat partner.

3. Setup an empty history.txt file.

4. Download a speed-to-text model from https://alphacephei.com/vosk/models and modify the settings in chatting.py accordingly.

5. Install everything from requirements.txt

6. Run ./chatting.py