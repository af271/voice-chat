import os
import sys
import openai
from time import time, sleep
from dotenv import load_dotenv
from datetime import datetime
from synthesize import PollySynthesizer


load_dotenv(verbose=True)


def now(offset=0):
    return datetime.fromtimestamp(time() + offset).strftime('%Y-%m-%d %H:%M:%S')


class Responder:

    def __init__(self, **kwargs):
        self.response = None

    def generate(self, messages):
        for i in range(10):
            token = str(i) + " "
            yield token
            sleep(0.5)

    def display_token(self, token):
        sys.stdout.write(token)
        sys.stdout.flush()

    def send_response(self, messages, response):
        print("Received messages:\n" + messages)
        print("Response: ", response)


class VoiceResponder(Responder):
    MAX_LEN = 5000

    def __init__(self, speaker="", responder="", voice="Joanna", **kwargs):
        openai.api_key = os.getenv("OPENAI_API_KEY")
        self.model_engine = os.getenv("OPENAI_MODEL_ENGINE")
        self.synthesizer = PollySynthesizer(voice_id=voice)
        self.speaker_name = speaker
        self.responder_name = responder

        # all instructions about the personality etc. are in role.txt
        with open("role.txt", "r") as f:
            self.role = f.read()

        # open history.txt for reading and writing
        with open("history.txt", "r") as f:
            self.context = f.read()
        self.context = self.context[-self.MAX_LEN:]

        print(self.role)
        sys.stdout.write(self.context[:-1])

        self.response = None

    def generate(self, messages):
        message1 = f"[{self.speaker_name}]: {messages}\n"  # {now()} 
        prompt = self.context + message1 + f"[{self.responder_name}]: "  # {now(10)} 
        os.system("clear")
        print(f"ROLE: {self.role}")
        print(f"\nCONVERSATION (~{int(len(prompt) / 4)} tokens):")
        # print prompt without new line
        sys.stdout.write(prompt)
        msg = [{"role": "system", "content": self.role}] + [{"role": "user", "content": prompt}]
        try:
            # response = ""
            # gen = openai.Completion.create(model=self.model_engine, prompt=prompt, max_tokens=2048, stream=True)
            for resp in openai.ChatCompletion.create(model=self.model_engine, messages=msg, max_tokens=2048, stream=True):
                # token = resp.choices[0].text
                delta = resp.choices[0].delta
                if len(delta) == 0:
                    break
                token = delta.content
                if len(token) > 0 and token[0] == "\n":
                    token = token[1:]
                # response += token
                # idx = response.find("[" + self.speaker_name + "]")
                # if idx != -1:
                #     response = response[:idx]
                yield token
        except Exception as e:
            print(e)
            print(prompt)
            print(len(prompt))
            import pdb
            pdb.set_trace()

    def display_token(self, token):
        sys.stdout.write("\033[0;32m" + token + "\033[0;0m")  # green
        sys.stdout.flush()

    def send_response(self, messages, response):
        message1 = f"[{self.speaker_name}]: {messages}\n"
        message2 = f"[{self.responder_name}]: {response}\n"

        self.context += message1 + message2

        # maintain only the last MAX_LEN characters
        self.context = self.context[-self.MAX_LEN:]

    def terminate(self):
        # write to history.txt
        with open("history.txt", "w") as f:
            f.write(self.context)
        
