#!/usr/bin/env python3
import os
import openai
import pyaudio
from dotenv import load_dotenv
from time import time
from datetime import datetime
from vosk import Model, KaldiRecognizer
from synthesize import PollySynthesizer

load_dotenv(verbose=True)

langauge = "russian"
settings_dict = {
    "english": {"model": "vosk-model-en-us-0.22", "voice": "Joanna", "sender": "Jenny", "receiver": "Arthur"},
    "russian": {"model": "vosk-model-small-ru-0.22", "voice": "Tatyana", "sender": "Таня", "receiver": "Артур"}
}
settings = settings_dict[langauge]

openai.api_key = os.getenv("OPENAI_API_KEY")
model_engine = os.getenv("OPENAI_MODEL_ENGINE")
synthesizer = PollySynthesizer(voice_id=settings["voice"])

# all instructions about the personality etc. are in role.txt
with open("role.txt", "r") as f:
    role = f.read()

def ask_chat_gpt(text: str) -> str:
    messages = [{'role': 'system', 'content': role}, {'role': 'user', 'content': text}]
    response = openai.ChatCompletion.create(
        model=model_engine,
        messages=messages,
    )
    content = response['choices'][0]['message']['content']
    return content


def now(offset=0):
    return datetime.fromtimestamp(time() + offset).strftime('%Y-%m-%d %H:%M:%S')


model = Model(settings["model"])
recognizer = KaldiRecognizer(model, 16000)

mic = pyaudio.PyAudio()
stream = mic.open(format=pyaudio.paInt16, channels=1, rate=16000, input=True, frames_per_buffer=8192)
# , input_device_index=7, output_device_index=7
stream.start_stream()

# open history.txt for reading and writing
f = open("history.txt", "r+")
context = f.read()

MAX_LEN = 15000
print(role)
print(context)
try:
    while True:
        data = stream.read(1024)

        if not recognizer.AcceptWaveform(data):
            continue

        # with open(filename, "rb") as audio_file:
        #     transcript = openai.Audio.transcribe("whisper-1", audio_file).text
        #     print(transcript)

        transcript = recognizer.Result()[14:-3]
        if transcript[:4] == "the ":
            transcript = transcript[4:]
        if transcript[-3:] == "the":
            transcript = transcript[:-3]

        if len(transcript) < 5:
            continue
        print(transcript)
        
        # get date and time in readable format
        message1 = f"{now()} [{settings['receiver']}]: {transcript}\n"
        print(message1)

        response = ask_chat_gpt(context + message1 + f"{now(10)} [Sender]: ")
        idx = response.find(str(datetime.now().year))
        if idx != -1:
            response = response[:idx]

        message2 = f"{now()} [{settings['sender']}]: {response}\n"
        print(message2)

        # don't listen while speaking (to avoid audio feedback)
        # stream.stop_stream()
        synthesizer.speak(response)
        # stream.start_stream()

        # write to history.txt
        # f.write(message1)
        # f.write(message2)
        context += message1 + message2

        # maintain only the last MAX_LEN characters
        context = context[-MAX_LEN:]

except KeyboardInterrupt:
   pass        
finally:
    f.close()
    stream.stop_stream()
    stream.close()
    mic.terminate()