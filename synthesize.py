import os
from boto3 import Session
from contextlib import closing
from elevenlabs import generate, play, set_api_key


class PollySynthesizer:

    def __init__(self, voice_id="Joanna"):
        # Create a client using the credentials and region defined in the [default]
        # section of the AWS credentials file (~/.aws/credentials).
        session = Session(profile_name="default")
        self.polly = session.client("polly")
        self.voice_id = voice_id

    def speak(self, text):
        # Request speech synthesis
        response = self.polly.synthesize_speech(Text=text, OutputFormat="mp3", VoiceId=self.voice_id)

        # Access the audio stream from the response
        if "AudioStream" not in response:
            return
        # Note: Closing the stream is important because the service throttles on the
        # number of parallel connections. Here we are using contextlib.closing to
        # ensure the close method of the stream object will be called automatically
        # at the end of the with statement's scope.
        with closing(response["AudioStream"]) as stream:
            play(stream.read())
            

class ElevenLabsSynthesizer:

    def __init__(self):
        set_api_key(os.getenv("ELEVEN_API_KEY"))

    def speak(self, text):
        # text-to-speech
        audio = generate(
            text=text,
            voice="Bella",
            model="eleven_monolingual_v1"
        )
        play(audio)
